package ssm.view;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.StandardCopyOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.DEFAULT_SLIDE_SHOW_HEIGHT;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.LABEL_SLIDE_SHOW_TITLE;
import static ssm.StartupConstants.PATH_CSS;
import static ssm.StartupConstants.PATH_CSS2;
import static ssm.StartupConstants.PATH_IMG;
import static ssm.StartupConstants.PATH_JS;
import static ssm.StartupConstants.PATH_SITE;
import static ssm.StartupConstants.PATH_SITE_ICONS;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 * This class provides the UI for the slide show viewer, note that this class is
 * a window and contains all controls inside.
 *
 * @author McKilla Gorilla & Charles Hirsch
 */
public class SlideShowViewer extends Stage {

    // THE MAIN UI

    SlideShowMakerView parentView;

    // THE DATA FOR THIS SLIDE SHOW
    SlideShowModel slides;
    public static String SLASH2 = "/";
    public static String HTML_NAME1 = "index";
    public static String HTML_EXT = ".html";
    public static String JSON_NAME = "slideData";
    public static String JSON_EXT = ".json";

    // HERE ARE OUR UI CONTROLS
    BorderPane borderPane;
    FlowPane topPane;
    Label slideShowTitleLabel;
    ImageView slideShowImageView;
    VBox bottomPane;
    Label captionLabel;
    FlowPane navigationPane;
    Button previousButton;
    Button nextButton;

    /**
     * This constructor just initializes the parent and slides references, note
     * that it does not arrange the UI or start the slide show view window.
     *
     * @param initParentView Reference to the main UI.
     */
    public SlideShowViewer(SlideShowMakerView initParentView) {
	// KEEP THIS FOR LATER
	parentView = initParentView;

	// GET THE SLIDES
	slides = parentView.getSlideShow();
    }

    /**
     * This method initializes the UI controls and opens the window with the
     * first slide in the slideshow displayed.
     */
    public void startSlideShow() /*throws IOException*/ {
        // (cahirsc) first create directories and files and then show html file
        
        String siteFilePath = PATH_SITE + slides.getTitle();
        String cssDir = siteFilePath + SLASH2 + PATH_CSS2;
        String jsDir = siteFilePath + SLASH2 + PATH_JS;
        String img = siteFilePath + SLASH2 + PATH_IMG;
        String html = siteFilePath + SLASH2 + HTML_NAME1 + HTML_EXT;
        String iconsDir = siteFilePath + SLASH2 + PATH_SITE_ICONS;
        String jsonFile =  PATH_SLIDE_SHOWS + slides.getTitle() + JSON_EXT;
        String jsonDest = siteFilePath + SLASH2 + JSON_NAME + JSON_EXT;
        Path source;
        Path dest;
        CopyOption[] options;
        File dirA = new File(siteFilePath);
        File dirD = new File(img);
        if (!dirA.exists()) {           //(cahirsc) This entire if statement sets up the directory
            dirA.mkdirs();              // after this, things that are allowed to change are added
            File dirB = new File(cssDir);
            File dirC = new File(jsDir);            
            File dirE = new File(iconsDir);
            File htmlFile = new File(html);
            dirB.mkdirs();
            dirC.mkdirs();            
            dirE.mkdirs();
            //File a = File("data/SlideShowStyle.css");
            //File source = new File("data/SlideShowStyle.css");
            //File dest = new File(cssDir + "SlideShowStyle.css");
            source = Paths.get("data/SlideShowStyle.css");
            dest = Paths.get(cssDir + "SlideShowStyle.css");
            options = new CopyOption[]{
                StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.COPY_ATTRIBUTES
            }; 
            try {
                java.nio.file.Files.copy(source, dest, options);
                //FileUtils.copyDirectory(source, dest);
                //BufferedWriter b1 = new BufferedWriter(new FileWriter(htmlFile));
                //b1.write();
            } catch (IOException ex) {
                Logger.getLogger(SlideShowViewer.class.getName()).log(Level.SEVERE, null, ex);
            }
            source = Paths.get("data/slideshow.js");
            dest = Paths.get(jsDir + "slideshow.js");
            try {
                java.nio.file.Files.copy(source, dest, options);
            } catch (IOException ex) {
                Logger.getLogger(SlideShowViewer.class.getName()).log(Level.SEVERE, null, ex);
            }
            source = Paths.get("images/icons/Play.png");
            dest = Paths.get(iconsDir + "Play.png");
            try {
                java.nio.file.Files.copy(source, dest, options);
            } catch (IOException ex) {
                Logger.getLogger(SlideShowViewer.class.getName()).log(Level.SEVERE, null, ex);
            }
            source = Paths.get("images/icons/Pause.png");
            dest = Paths.get(iconsDir + "Pause.png");
            try {
                java.nio.file.Files.copy(source, dest, options);
            } catch (IOException ex) {
                Logger.getLogger(SlideShowViewer.class.getName()).log(Level.SEVERE, null, ex);
            }
            source = Paths.get("images/icons/Next.png");
            dest = Paths.get(iconsDir + "Next.png");
            try {
                java.nio.file.Files.copy(source, dest, options);
            } catch (IOException ex) {
                Logger.getLogger(SlideShowViewer.class.getName()).log(Level.SEVERE, null, ex);
            }   
            source = Paths.get("images/icons/Previous.png");
            dest = Paths.get(iconsDir + "Previous.png");
            try {
                java.nio.file.Files.copy(source, dest, options);
            } catch (IOException ex) {
                Logger.getLogger(SlideShowViewer.class.getName()).log(Level.SEVERE, null, ex);
            }
            source = Paths.get("data/index.html");
            dest = Paths.get(siteFilePath + "/index.html");
            try {
                java.nio.file.Files.copy(source, dest, options);
            } catch (IOException ex) {
                Logger.getLogger(SlideShowViewer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            dirD.delete();
        }
        dirD.mkdirs();
        ObservableList<Slide> slidesArr = slides.getSlides();
        Slide s;
        // (cahirsc) This for loop adds all the images to the IMG file.
        for(int i = 0; i < slidesArr.size(); i++) {
            s = slidesArr.get(i);
            source = Paths.get(s.getImagePath() + s.getImageFileName());
            dest = Paths.get(img + s.getImageFileName());
            options = new CopyOption[]{
                StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.COPY_ATTRIBUTES
            };
            try {
                java.nio.file.Files.copy(source, dest, options);
            } catch (IOException ex) {
                Logger.getLogger(SlideShowViewer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // (cahirsc) And this adds the JSON file.
        source = Paths.get(jsonFile);
        dest = Paths.get(jsonDest);
        options = new CopyOption[]{
            StandardCopyOption.REPLACE_EXISTING,
            StandardCopyOption.COPY_ATTRIBUTES
        }; 
        try {
            java.nio.file.Files.copy(source, dest, options);
        } catch (IOException ex) {
            Logger.getLogger(SlideShowViewer.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
	// FIRST THE TOP PANE
        //VBox root = new VBox();
        String url = siteFilePath + "/index.html";
        URI uri = java.nio.file.Paths.get(url).toAbsolutePath().toUri();
        //URL iFile = getClass().getResource(url);
        //Scene scene = new Scene(new Group());
        WebView browser = new WebView();
        browser.getEngine().load(uri.toString());

	// NOW PUT STUFF IN THE STAGE'S SCENE
	Scene scene = new Scene(browser, 1000, 700);
        //scene.setRoot(scrollPane);
	setScene(scene);
        setMaximized(true);
	showAndWait();
    }

    // HELPER METHOD
    private void reloadSlideShowImageView() {
	try {
	    Slide slide = slides.getSelectedSlide();
	    if (slide == null) {
		slides.setSelectedSlide(slides.getSlides().get(0));
	    }
	    slide = slides.getSelectedSlide();
	    String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
	    File file = new File(imagePath);
	    
	    // GET AND SET THE IMAGE
	    URL fileURL = file.toURI().toURL();
	    Image slideImage = new Image(fileURL.toExternalForm());
	    slideShowImageView.setImage(slideImage);

	    // AND RESIZE IT
	    double scaledHeight = DEFAULT_SLIDE_SHOW_HEIGHT;
	    double perc = scaledHeight / slideImage.getHeight();
	    double scaledWidth = slideImage.getWidth() * perc;
	    slideShowImageView.setFitWidth(scaledWidth);
	    slideShowImageView.setFitHeight(scaledHeight);
	} catch (Exception e) {
	    // CANNOT SHOW A SLIDE SHOW WITHOUT ANY IMAGES
	    parentView.getErrorHandler().processError(LanguagePropertyType.ERROR_NO_SLIDESHOW_IMAGES);
	}
    }

    private void reloadCaption() {
	Slide slide = slides.getSelectedSlide();
	captionLabel.setText(slide.getCaption());
    }
}
